# NY Times News

A simple news app that retrieves latest news from New York times using.

## Getting Started

Clone the project from the Git repository and apply the following:

- Sync Gradle
- Clean Project
- Rebuild Project

### Prerequisites

[Android Studio](https://developer.android.com/studio/)

### Installing

Click on Run button, you should be able to choose between a connected android device or a configured emulator.

## Running the istrumented test

Right click the ExampleInstrumentedTest.java and click Run 'ExampleInstrumentedTest'

### Instrumented test description

The instrumented test consists of the below

- Swipe to refresh and fetch news

### Generating a coverage report

To get a lint report, execute the below command in Terminal window of Android Studio:

```
./gradlew lint
```

The report will be generated at the following path:
app/build/outputs/reports/

## Authors

**Joseph Hajj**
