package com.nytimesnews.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MediaMetadataBean implements Serializable {

    @SerializedName("url")
    public String url = "";
    @SerializedName("format")
    public String format = "";
    @SerializedName("height")
    public Integer height;
    @SerializedName("width")
    public Integer width;

}
