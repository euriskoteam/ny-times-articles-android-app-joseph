package com.nytimesnews.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ResultsArrayBean implements Serializable {

    @SerializedName("url")
    public String url = "";
    @SerializedName("adx_keywords")
    public String adx_keywords = "";
    @SerializedName("column")
    public String column = "";
    @SerializedName("section")
    public String section = "";
    @SerializedName("byline")
    public String byline = "";
    @SerializedName("type")
    public String type = "";
    @SerializedName("title")
    public String title = "";
    @SerializedName("abstract")
    public String abstractText = "";
    @SerializedName("published_date")
    public String published_date = "";
    @SerializedName("source")
    public String source = "";
    @SerializedName("id")
    public Float id;
    @SerializedName("asset_id")
    public Float asset_id;
    @SerializedName("views")
    public Integer views;
    @SerializedName("des_facet")
    public Object des_facet;
    @SerializedName("org_facet")
    public Object org_facet;
    @SerializedName("per_facet")
    public Object per_facet;
    @SerializedName("geo_facet")
    public Object geo_facet;
    @SerializedName("media")
    public ArrayList<MediaBean> media = new ArrayList<>();

}
