package com.nytimesnews.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class MediaBean implements Serializable {

    @SerializedName("type")
    public String type = "";
    @SerializedName("subtype")
    public String subtype = "";
    @SerializedName("caption")
    public String caption = "";
    @SerializedName("copyright")
    public String copyright = "";
    @SerializedName("approved_for_syndication")
    public Integer approved_for_syndication;
    @SerializedName("media-metadata")
    public ArrayList<MediaMetadataBean> mediaMetadata = new ArrayList<>();

}
