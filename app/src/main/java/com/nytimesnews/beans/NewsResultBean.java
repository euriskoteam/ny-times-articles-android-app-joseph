package com.nytimesnews.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class NewsResultBean implements Serializable {

    @SerializedName("status")
    public String status = "";
    @SerializedName("copyright")
    public String copyright = "";
    @SerializedName("num_results")
    public Integer num_results;
    @SerializedName("results")
    public ArrayList<ResultsArrayBean> results = new ArrayList<>();

}
