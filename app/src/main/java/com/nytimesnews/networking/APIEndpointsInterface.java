package com.nytimesnews.networking;

import com.nytimesnews.beans.NewsResultBean;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIEndpointsInterface {

    /*----- Get News -----*/
    @GET("7.json?")
    Call<NewsResultBean> getNews(@Query("api-key") String key);

}
