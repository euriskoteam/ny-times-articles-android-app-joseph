package com.nytimesnews.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nytimesnews.R;
import com.nytimesnews.beans.NewsResultBean;
import com.nytimesnews.beans.ResultsArrayBean;
import com.nytimesnews.utils.GlobalChangeActivity;
import com.nytimesnews.utils.ImageUtils;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    //region Variables Declaration

    //Global Utils
    Activity activity;

    //API Variables
    NewsResultBean newsResultBean;
    ArrayList<ResultsArrayBean> resultsArrayBeans;

    //Views Variables
    LayoutInflater inflater;
    ImageLoader imageLoader;
    DisplayImageOptions options;

    //endregion

    public NewsAdapter(Activity activity, NewsResultBean newsResultBean) {
        this.activity = activity;
        this.newsResultBean = newsResultBean;

        resultsArrayBeans = newsResultBean.results;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = ImageUtils.getImageLoader(activity);
        options = ImageUtils.getImageOptions(R.mipmap.ic_launcher);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = inflater.inflate(R.layout.item_news, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        ResultsArrayBean bean = resultsArrayBeans.get(i);

        holder.tvTitle.setText(bean.title);
        holder.tvPublishDate.setText(bean.published_date);
        holder.tvByLine.setText(bean.byline);
        imageLoader.displayImage(bean.media.get(0).mediaMetadata.get(0).url, holder.ivNews, options);

        holder.rlNewsItem.setTag(i);
        holder.rlNewsItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResultsArrayBean selectedNewsBean = resultsArrayBeans.get((Integer) v.getTag());
                GlobalChangeActivity.goToNewsDetails(activity, selectedNewsBean);
            }
        });
    }

    @Override
    public int getItemCount() {
        return resultsArrayBeans.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvPublishDate, tvByLine;
        ImageView ivNews;
        RelativeLayout rlNewsItem;

        ViewHolder(View view) {
            super(view);
            tvTitle = view.findViewById(R.id.tvTitle);
            tvPublishDate = view.findViewById(R.id.tvPublishDate);
            tvByLine = view.findViewById(R.id.tvByLine);
            ivNews = view.findViewById(R.id.ivNews);
            rlNewsItem = view.findViewById(R.id.rlNewsItem);
        }
    }

}
