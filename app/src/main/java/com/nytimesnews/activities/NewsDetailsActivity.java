package com.nytimesnews.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nytimesnews.R;
import com.nytimesnews.beans.ResultsArrayBean;
import com.nytimesnews.utils.GlobalVars;
import com.nytimesnews.utils.ImageUtils;

import androidx.appcompat.app.AppCompatActivity;

public class NewsDetailsActivity extends AppCompatActivity {

    //region Variables Declaration

    //Global Utils
    Activity activity;

    //API Variables
    ResultsArrayBean resultsArrayBean;

    //Views Variables
    ImageView ivNewsHeader;
    TextView tvNewsTitle, tvNewsByLine, tvNewsDate, tvNewsDescription;
    ImageLoader imageLoader;
    DisplayImageOptions options;

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);

        activity = NewsDetailsActivity.this;

        resultsArrayBean = (ResultsArrayBean) getIntent().getSerializableExtra(GlobalVars.BUNDLE_NEWS_RESULT);

        defineViews();

        handleViewContent();
    }

    //region View Initialization
    private void defineViews() {
        ivNewsHeader = findViewById(R.id.ivNewsHeader);
        tvNewsTitle = findViewById(R.id.tvNewsTitle);
        tvNewsByLine = findViewById(R.id.tvNewsByLine);
        tvNewsDate = findViewById(R.id.tvNewsDate);
        tvNewsDescription = findViewById(R.id.tvNewsDescription);

        imageLoader = ImageUtils.getImageLoader(activity);
        options = ImageUtils.getImageOptions(R.mipmap.ic_launcher);
    }

    private void handleViewContent() {
        imageLoader.displayImage(resultsArrayBean.media.get(0).mediaMetadata.get(0).url, ivNewsHeader, options);
        tvNewsTitle.setText(resultsArrayBean.title);
        tvNewsByLine.setText(resultsArrayBean.byline);
        tvNewsDate.setText(resultsArrayBean.published_date);
        tvNewsDescription.setText(resultsArrayBean.abstractText);
    }
    //endregion
}
