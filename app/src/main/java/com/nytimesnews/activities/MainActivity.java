package com.nytimesnews.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.nytimesnews.R;
import com.nytimesnews.adapters.NewsAdapter;
import com.nytimesnews.beans.NewsResultBean;
import com.nytimesnews.networking.APIClient;
import com.nytimesnews.networking.APIEndpointsInterface;
import com.nytimesnews.utils.GlobalFunctions;
import com.nytimesnews.utils.GlobalVars;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    //region Variables Declaration

    //Global Utils
    Activity activity;
    Boolean isRefreshing = false;

    //API Variables
    APIEndpointsInterface apiEndpointsInterface;
    Call<NewsResultBean> newsResultBeanCall;

    //Views Variables
    SwipeRefreshLayout srlLayout;
    RecyclerView rvNewsList;
    TextView tvNoDataFound;

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fabric.with(this, new Crashlytics());

        activity = MainActivity.this;

        defineViews();

        handleViewFunctions();

        fetchNews();
    }

    //region View Initialization
    private void defineViews() {
        srlLayout = findViewById(R.id.srlLayout);
        rvNewsList = findViewById(R.id.rvNewsList);
        tvNoDataFound = findViewById(R.id.tvNoDataFound);
    }

    private void handleViewFunctions() {
        srlLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchNews();
            }
        });
    }

    //endregion

    //region API Call
    private void fetchNews() {
        if (GlobalFunctions.CheckNetwork(activity)) {
            if (!isRefreshing) {
                isRefreshing = true;
                srlLayout.setRefreshing(true);
                activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                apiEndpointsInterface = APIClient.getClient(GlobalVars.BASE_URL).create(APIEndpointsInterface.class);

                newsResultBeanCall = apiEndpointsInterface.getNews(GlobalVars.VAL_API_KEY);

                newsResultBeanCall.enqueue(new Callback<NewsResultBean>() {
                    @Override
                    public void onResponse(Call<NewsResultBean> call, Response<NewsResultBean> response) {
                        srlLayout.setRefreshing(false);
                        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        isRefreshing = false;
                        if (response.isSuccessful() && response.body().status.equals("OK")) {
                            handleResponse(response.body());
                        } else {
                            tvNoDataFound.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(Call<NewsResultBean> call, Throwable t) {
                        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        if (!call.isCanceled()) {
                            srlLayout.setRefreshing(false);
                            isRefreshing = false;

                            GlobalFunctions.showDialogActions(activity, getString(R.string.no_connection), getString(R.string.retry), getString(R.string.cancel), new Runnable() {
                                @Override
                                public void run() {
                                    fetchNews();
                                }
                            }, null);

                            GlobalFunctions.printException(t);
                        }
                    }
                });
            }
        } else {
            GlobalFunctions.showDialogActions(activity, getString(R.string.no_connection), getString(R.string.retry), getString(R.string.cancel), new Runnable() {
                @Override
                public void run() {
                    fetchNews();
                }
            }, null);
        }
    }

    private void handleResponse(NewsResultBean newsResultBean) {
        if (newsResultBean != null) {
            try {
                NewsAdapter newsAdapter = new NewsAdapter(activity, newsResultBean);

                rvNewsList.setLayoutManager(new LinearLayoutManager(activity));
                rvNewsList.setAdapter(newsAdapter);
            } catch (Exception e) {
                GlobalFunctions.printException(e);
            }
        }
    }
    //endregion

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (newsResultBeanCall != null) {
            newsResultBeanCall.cancel();
        }
    }
}
