package com.nytimesnews.utils;

import android.app.Activity;
import android.graphics.Bitmap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class ImageUtils {

    public static ImageLoader getImageLoader(Activity activity) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(ImageLoaderConfiguration.createDefault(activity));
        }
        return imageLoader;
    }


    public static DisplayImageOptions getImageOptions(int defaultIcon) {
        return new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .showImageOnLoading(defaultIcon)
                .showImageOnFail(defaultIcon)
                .showImageForEmptyUri(defaultIcon)
                .build();
    }

}
