package com.nytimesnews.utils;

import android.app.Activity;
import android.content.Intent;

import com.nytimesnews.activities.NewsDetailsActivity;
import com.nytimesnews.beans.ResultsArrayBean;

public class GlobalChangeActivity {

    public static void goToNewsDetails(Activity activity, ResultsArrayBean resultsArrayBean) {
        Intent intent = new Intent(activity, NewsDetailsActivity.class);
        intent.putExtra(GlobalVars.BUNDLE_NEWS_RESULT, resultsArrayBean);
        activity.startActivity(intent);
    }

}
