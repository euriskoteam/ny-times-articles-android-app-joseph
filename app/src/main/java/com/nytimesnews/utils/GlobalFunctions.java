package com.nytimesnews.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.os.Handler;

public class GlobalFunctions {

    public static AlertDialog showDialogActions(final Activity activity, String msg, String positiveText, String negativeText, final Runnable positiveRunnable, final Runnable negativeRunnable) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setMessage(msg);
        dialog.setCancelable(false);
        dialog.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                new Handler().post(positiveRunnable);
            }
        });
        dialog.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                new Handler().post(negativeRunnable);
            }
        });
        return activity.isFinishing() ? null : dialog.show();
    }

    public static void printException(Throwable e) {
        if (e != null) {
            Log.d(GlobalVars.LOG_TAG, Log.getStackTraceString(e));
        }
    }

    public static boolean CheckNetwork(Activity activity) {
        try {
            ConnectivityManager connectivity = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null)
                    for (int i = 0; i < info.length; i++)
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
            }
            return false;
        } catch (Exception e) {
            printException(e);
        }
            return false;
    }
}

