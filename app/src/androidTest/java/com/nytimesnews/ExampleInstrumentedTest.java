package com.nytimesnews;

import android.app.Activity;
import android.view.View;

import com.nytimesnews.activities.MainActivity;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.NoMatchingViewException;
import androidx.test.espresso.ViewAssertion;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    private int itemCount = 0;
    Activity activity;
    RecyclerView recyclerView;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);


    @Before
    public void init(){
        this.activity = mActivityRule.getActivity();
        this.recyclerView = activity.findViewById(R.id.rvNewsList);
    }

    @Test
    public void recyclerViewNewsClickTest() {
        Espresso.onView(ViewMatchers.withId(R.id.srlLayout))
                .perform(ViewActions.swipeDown());

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (recyclerView.getAdapter() != null) {
            this.itemCount = recyclerView.getAdapter().getItemCount();
            for (int i = 0; i < itemCount; i++) {
                Espresso.onView(ViewMatchers.withId(R.id.rvNewsList)).perform(RecyclerViewActions.actionOnItemAtPosition(
                        i, ViewActions.click()));
                Espresso.pressBack();
            }
        }

    }


    public static ViewAssertion hasItemsCount() {
        return new ViewAssertion() {
            @Override
            public void check(View view, NoMatchingViewException e) {
                if (!(view instanceof RecyclerView)) {
                    throw e;
                }
                RecyclerView rv = (RecyclerView) view;
                if (rv.getAdapter() != null) {
                    Assert.assertTrue(rv.getAdapter().getItemCount() > 0);
                }
            }
        };
    }

}
